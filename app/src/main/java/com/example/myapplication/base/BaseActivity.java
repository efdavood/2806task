package com.example.myapplication.base;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public abstract class BaseActivity extends AppCompatActivity {


    public abstract int layoutResId();

    public void showSnackBar(String message, int duration) {
        Snackbar.make(findViewById(layoutResId()), message, duration).show();
    }

}
