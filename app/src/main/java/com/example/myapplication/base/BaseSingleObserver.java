package com.example.myapplication.base;

import android.util.Log;

import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public abstract class BaseSingleObserver<T> implements SingleObserver<T> {
    private static final String TAG = "BaseSingleObserver";

    @Override
    public void onError(@NonNull Throwable e) {
        Log.i(TAG, "onError: " + e.toString());
    }
}
