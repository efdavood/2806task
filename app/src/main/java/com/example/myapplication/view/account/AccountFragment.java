package com.example.myapplication.view.account;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseFragment;
import com.example.myapplication.base.BaseSingleObserver;
import com.example.myapplication.model.User;
import com.example.myapplication.view.MainActivity;
import com.example.myapplication.view.order.OrderFragment;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AccountFragment extends BaseFragment implements View.OnClickListener {
    // views
    private ImageView ivProfile;
    private MaterialTextView tvName;
    private MaterialTextView tvPhoneNumber;
    private MaterialTextView tvDoneCount;
    private MaterialTextView tvDoingCount;
    private MaterialTextView tvCanceledCount;
    private MaterialTextView tvCredit;
    // objects
    private AccountViewModel viewModel;
    private CompositeDisposable disposable;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.getUserDetail().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSingleObserver<User>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.annotations.NonNull User user) {
                        Picasso.get().load(user.getUserProfile()).into(ivProfile);
                        tvName.setText(user.getUserName());
                        tvPhoneNumber.setText(user.getUserPhoneNumber());
                        tvDoneCount.setText(user.getDoneCount());
                        tvDoingCount.setText(user.getDoingCount());
                        tvCanceledCount.setText(user.getCanceledCount());
                        tvCredit.setText(user.getCredit());
                    }
                });
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_account;
    }

    @Override
    public MainActivity activity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void initViews(View view) {
        // views
        ivProfile = view.findViewById(R.id.iv_accountFragment_avatar);
        tvName = view.findViewById(R.id.mtv_accountFragment_name);
        tvPhoneNumber = view.findViewById(R.id.mtv_accountFragment_phoneNumber);
        tvDoneCount = view.findViewById(R.id.mtv_accountFragment_doneCount);
        tvDoingCount = view.findViewById(R.id.mtv_accountFragment_doing);
        tvCanceledCount = view.findViewById(R.id.mtv_accountFragment_canceledCount);
        tvCredit = view.findViewById(R.id.mtv_accountFragment_credit);
        // taxi, truck, motorcycle, motorcycleTaxi
        view.findViewById(R.id.mcv_accountFragment_taxi).setOnClickListener(this);
        view.findViewById(R.id.mcv_accountFragment_truck).setOnClickListener(this);
        view.findViewById(R.id.mcv_accountFragment_motorcycle).setOnClickListener(this);
        view.findViewById(R.id.mcv_accountFragment_motorcycleTaxi).setOnClickListener(this);
        // objects
        viewModel = new AccountViewModel();
        disposable = new CompositeDisposable();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

    @Override
    public void onClick(View v) {
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction().add(R.id.root_main, new OrderFragment()).commit();
    }
}
