package com.example.myapplication.view.account;

import com.example.myapplication.base.BaseViewModel;
import com.example.myapplication.model.User;
import com.example.myapplication.model.api.ApiService;
import com.example.myapplication.model.api.ApiServiceProvider;

import io.reactivex.Single;

public class AccountViewModel extends BaseViewModel {

    AccountViewModel(){
        initViewModel();
    }

    public Single<User> getUserDetail() {
        return apiService.getUserDetail();
    }
}
