package com.example.myapplication.view.order;

import android.view.View;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseFragment;
import com.example.myapplication.view.MainActivity;
import com.example.myapplication.view.detail.DetailFragment;

public class OrderFragment extends BaseFragment {

    @Override
    public int layoutRes() {
        return R.layout.fragment_order;
    }

    @Override
    public MainActivity activity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void initViews(View view) {
        view.findViewById(R.id.btn_orderFragment_addPoint).setOnClickListener(v -> {
            new DetailFragment().show(getChildFragmentManager(), null);
        });
    }
}
